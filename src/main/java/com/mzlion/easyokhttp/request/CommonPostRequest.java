/*
 * Copyright (C) 2016 mzlion(and.mz.yq@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.easyokhttp.request;

import com.mzlion.core.lang.CollectionUtils;
import com.mzlion.core.lang.StringUtils;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * <p>
 * HTTP的POST提交封装对象，默认所有参数采用<code>application/x-www-form-urlencoded</code>编码后传递。
 * </p>
 * 该POST提交数据形如Web开发的常规表单提交：
 * <pre>
 *     &lt;form action="#" method="post" enctype="application/x-www-form-urlencoded"&gt;
 *         &lt;input type="hidden" name="id" value="1"&gt;
 *         vinput type="text" name="username" value="admin"&gt;
 *      &lt;/form&gt;
 * </pre>
 *
 * @author mzlion on 2016-04-16
 */
public class CommonPostRequest extends AbstractHttpRequest<CommonPostRequest> {

    /**
     * 表单提交的参数对列表
     */
    private Map<String, String> formParameters;

    /**
     * 默认构造器
     *
     * @param url 请求地址
     */
    public CommonPostRequest(String url) {
        super(url);
        this.formParameters = new IdentityHashMap<>();
    }

    /**
     * 设置提交的请求参数及其值
     *
     * @param name  参数名
     * @param value 参数值
     * @return {@linkplain CommonPostRequest}
     */
    public CommonPostRequest formParam(String name, String value) {
        if (StringUtils.hasText(name) && StringUtils.hasLength(value)) {
            this.formParameters.put(name, value);
        }
        return this;
    }

    /**
     * 设置提交的请求参数及其值
     *
     * @param parameters 键值对列表
     * @return {@linkplain CommonPostRequest}
     */
    public CommonPostRequest formParam(Map<String, String> parameters) {
        if (CollectionUtils.isNotEmpty(parameters)) {
            for (String name : parameters.keySet()) {
                if (StringUtils.hasLength(name) && StringUtils.hasText(parameters.get(name)))
                    this.formParameters.put(name, parameters.get(name));
            }
        }
        return this;
    }

    /**
     * 获取{@linkplain RequestBody}对象
     */
    @Override
    protected RequestBody generateRequestBody() {
        FormBody.Builder builder = new FormBody.Builder();
        if (CollectionUtils.isNotEmpty(this.formParameters)) {
            for (Map.Entry<String, String> entry : this.formParameters.entrySet()) {
                builder.add(entry.getKey(), entry.getValue());
            }
        }
        return builder.build();
    }

    /**
     * 根据不同的请求方式，将RequestBody转换成Request对象
     *
     * @param requestBody 请求体
     * @return {@link Request}
     * @see RequestBody
     */
    @Override
    protected Request generateRequest(RequestBody requestBody) {
        Request.Builder builder = new Request.Builder();
        builder.headers(super.buildHeaders());
        return builder.url(this.buildUrl()).post(requestBody).build();
    }
}
