# Change log

## Version(1.0.1-beta)2016-07-19
* Fix:修正将响应结果进行转换时的bug
* Update:结果转换划分，目前新的以`as`开头表示文本处理,`transferTo`则处理二进制处理
* Update:将`HttpClient.stringBody()`方法名更改为`HttpClient.textBody()`
* Add:完善文档

## Version(1.0.0-beta)2016-07-15
* Initial beta